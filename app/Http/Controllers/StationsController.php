<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Station;

class StationsController extends Controller
{
    public function show(){
        $stationsResult = Station::get();
        $stations = [];

        foreach ($stationsResult as $station){
          $tempArray = array( 'naam' => $station->stationsnaam, 'meldingen' => url('/api/meldingen/station/' . $station->id) , 'filters' => url('api/filters/' . $station->id) , 'positie' => array('lng' => $station->lng, 'lat' => $station->lat));
          array_push($stations, $tempArray);
        }

        return response()->json([
          'stations' => $stations,
        ]);
      }
}
