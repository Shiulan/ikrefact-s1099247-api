<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationFilter extends Model
{
    protected $table = "stationsfilters";
}
