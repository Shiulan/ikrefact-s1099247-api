<?php

use Illuminate\Database\Seeder;

class FilterCategorieenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       $categorieenArray = array(
         array('id' => 1, 'naam' => 'OV'),
         array('id' => 2, 'naam' => 'Service'),
         array('id' => 3, 'naam' => 'Shops'),
         array('id' => 4, 'naam' => 'Food'),
       );

         DB::table("filter_categorieen")->insert($categorieenArray);
     }
}
