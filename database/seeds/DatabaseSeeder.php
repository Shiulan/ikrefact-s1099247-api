<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FilterCategorieenSeeder::class);
        $this->call(StationsSeeder::class);
        $this->call(MeldingenSeeder::class);
        $this->call(FiltersSeeder::class);
        $this->call(StationFiltersSeeder::class);

    }
}
