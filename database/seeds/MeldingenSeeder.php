<?php

use Illuminate\Database\Seeder;

class MeldingenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

       $meldingenArray = array(
         array(
           "id"=> "1",
           "stations_id" => "1",
           "image" => "meldingen/bus.jpg",
           "titel"=> 'Minder bussen',
           "beschrijving"=> "Vandaag rijden er minder bussen i.v.m. stakingen.",
           "tekst"=> "Vandaag rijden er minder bussen i.v.m. stakingen van buschauffeurs. Houd er rekening mee dat uw bus wellicht niet rijdt vandaag. Morgen rijden alle bussen weer zoals u gewend bent. Onze excuses voor dit ongemak."
         ),
         array(
           "id"=> "2",
           "stations_id" => "2",
           "image" => "meldingen/trein.jpg",
           "titel"=> 'Aanrijding met persoon',
           "beschrijving"=> "De komende 2 uur rijden er geen trinen op het traject Den Haag CS - Leiden CS.",
           "tekst"=> "De trein op het traject Den Haag CS - Leiden CS heeft een aanrijding gehad. Hierdoor rijden er de komende 2 uur geen treinen op dit traject. Onze excuses voor het ongemak."
         ),
         array(
           "id"=> "3",
           "stations_id" => "1",
           "image" => "meldingen/wc.png",
           "titel"=> 'Wc verstopt',
           "beschrijving"=> "De wc's zijn buiten gebruik vandaag",
           "tekst"=> "Wegens verstopte wc's zijn deze tijdelijk buiten gebruik. Onze excuses voor dit ongemak. We werken er hard aan om dit probleem zo snel mogelijk op te lossen."
         ),
       );

         DB::table("meldingen")->insert($meldingenArray);
     }
}
