<?php

use Illuminate\Database\Seeder;

class StationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $stationsArray =array(
        array(
          "id"=> "1",
          "stationsnaam"=> "Leiden CS",
          "lat" => "52.166417", "lng" => "4.482079",
        ),
        array(
          "id"=> "2",
          "stationsnaam"=> "Leiden Lammenschans",
          "lat" => "52.1467", "lng" => "4.4928",
        ),
        array(
          "id"=> "3",
          "stationsnaam"=> "Utrecht CS",
          "lat" => "52.0894", "lng" => "5.10982",
        ),
        array(
          "id"=> "4",
          "stationsnaam"=> "Den Haag CS",
          "lat" => "52.0803", "lng" => "4.3252",
        ),
        array(
          "id"=> "5",
          "stationsnaam"=> "Leiden De Vink",
          "lat" => "52.1471", "lng" => "4.4564",
        ),
        array(
          "id"=> "6",
          "stationsnaam"=> "Amsterdam CS",
          "lat" => "52.364553", "lng" => "4.909876"
        ),
        array(
          "id"=> "7",
          "stationsnaam"=> "Amersfoort CS",
          "lat" => "52.1529", "lng" => "5.37299",
        ),
        array(
          "id"=> "8",
          "stationsnaam"=> "Tilburg CS",
          "lat" => "51.5614", "lng" => "5.08108",
        ),
        array(
          "id"=> "9",
          "stationsnaam"=> "Rotterdam CS",
          "lat" => "51.925", "lng" => "4.46869",
        ),
    );

        DB::table("stations")->insert($stationsArray);
    }
}
